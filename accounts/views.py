# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.conf import settings
from django.core.mail import send_mail
import json, sys

# Create your views here.

def signIn(request):
    msg = ""
    error_status = False
    if request.method == "POST":
        try:
            email = request.POST.get("email")
            password = request.POST.get("password")
            user_list = User.objects.filter(email=email).count()
            if user_list > 0:
                objU = User.objects.get(email=email)
                user = authenticate(username=objU.username, password=password)
                if user is not None:
                    login(request, user)
                    return redirect("/")
                else:
                    msg = "User is not authenticated."
                    error_status = True
            else:
                msg = "Email address not matched."
                error_status = True
        except:
            msg = "Error occured while user login."
            error_status = True

    return render(request, 'login.html', {
        "msg": msg,
        "error_status": error_status
    })

def signUp(request):
    msg = ""
    success_status = False
    error_status = False
    objU = None
    if request.method == "POST":
        try:
            firstName = request.POST.get("firstName")
            lastName = request.POST.get("lastName")
            email = request.POST.get("email")
            password = request.POST.get("password")
            if User.objects.filter(email=email).count() > 0:
                msg = "Email address already exist."
                error_status = True
            elif User.objects.filter(username=firstName+lastName).count() > 0:
                objU = User.objects.get(username=firstName+lastName)
                msg = "Username already exist."
                error_status = True
            else:
                # objNU is an instance for User model.
                objNU = User()
                objNU.username = firstName + lastName
                objNU.first_name=firstName
                objNU.last_name=lastName
                objNU.email=email
                objNU.set_password(password)
                objNU.is_active = True
                objNU.save()
                msg = 'User registered successfully.'
                success_status = True
        except:
            error_status = True
            msg = "Error occured while user register."

    return render(request, 'register.html', {
        "objU": objU,
        "msg": msg,
        "success_status": success_status,
        "error_status": error_status
        })

def forgot_password(request):
    msg = ""
    success_status = False
    error_status = False
    if request.method == "POST":
        print("Going to send email : ")
        email = request.POST.get("email")
        print(email)
        # objU is an instance for User model.
        objU = User.objects.get(email=str(email))
        if objU:
            subject = "Forget Password"
            message = "Hello,"
            email_from = settings.EMAIL_HOST_USER
            email_to = email
            html = ""
            html += "<html>"
            html += "<body>"
            html += "<div id='container'>"
            html += "<p>Please click on the following link : </p>"
            html += "<br/>"
            html += "<a href='http://127.0.0.1:8000/reset-password/?q=" + email + "' >Click this link</a>"
            html += "</div>"
            html += "</body>"
            html += "</html>"
            send_mail(subject, message, email_from, [email_to], html_message=html)
            msg = "Email send successfully."
            success_status = True
        else:
            msg = "Email address is not matched."
            error_status = True

    return render(request, 'forgot-password.html', {
        "msg": msg,
        "success_status": success_status,
        "error_status": error_status
    })

def reset_password(request):
    msg = ""
    success_status = False
    error_status = False
    email = None
    if request.method == "GET":
        email = request.GET.get("q")
    if request.method == "POST":
        try:
            email = request.POST.get("email")
            password = request.POST.get("password")
            if User.objects.filter(email=email).count() > 0:
                # objU is an instance for User model.
                objU = User.objects.get(email=email)
                objU.set_password(password)
                objU.save()
                success_status = True
                msg = "Password reset successfully."
            else:
                error_status = True
                msg = "Email address not matched."
        except:
            error_status = True
            msg = "Error occured while password reset."

    return render(request, 'reset-password.html', {
        "email": email,
        "msg": msg,
        "success_status": success_status,
        "error_status": error_status
    })


def logout_user(request):
    msg = ""
    error_status = False
    success_status = False
    if request.method == "POST":
        try:
            logout(request)
            msg = "User logout successfully."
            success_status = True
        except:
            msg = "Error occured while logout."
            error_status = True

    return HttpResponse(json.dumps({
        "msg": msg,
        "success_status": success_status,
        "error_status": error_status
    }), content_type="application/json")

def tables(request):

    return render(request, 'tables.html', {})

def charts(request):
     
    return render(request, 'charts.html', {})
