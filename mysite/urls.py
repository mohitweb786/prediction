from django.conf.urls import include, url
from django.contrib import admin
from home import views as home_views
from accounts import views as account_views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', home_views.index, name='Home'),
    url(r'^prediction/', include('prediction.urls')),

    url(r'^sign-in/$', account_views.signIn, name='Login'),
    url(r'^sign-up/$', account_views.signUp, name='SignUp'),
    url(r'^logout/$', account_views.logout_user, name='Logout'),
    url(r'^forgot-password/$', account_views.forgot_password, name='Forgot Password'),
    url(r'^reset-password/$', account_views.reset_password, name='Reset Password'),
    # url(r'^tables/$', account_views.tables, name='Tables'),
    url(r'^charts/$', account_views.charts, name='Charts'),

]