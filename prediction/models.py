# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Prediction(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name="prediction")
    # name = models.CharField(max_length=200)
    prediction = models.BooleanField(default=False)
    weather_man_prediction = models.BooleanField(default=False)
    outcomes = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
