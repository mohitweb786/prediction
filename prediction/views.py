# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
import json, datetime
from .models import Prediction

# Create your views here.

@login_required
def admin_list_prediction(request):
    return render(request, 'admin/prediction_list.html', {
        "predictions": Prediction.objects.all()
    })

@login_required
def admin_add_update_prediction(request):
    msg = ""
    error_status = False
    success_status = False
    objP = None
    if request.method == "GET":
        prediction_id = request.GET.get("id")
        objP = Prediction.objects.get(pk=int(prediction_id))
    
    if request.method=='POST':
        try:
            # name=request.POST['name']
            # date=request.POST['date']
            prediction=request.POST['prediction']
            weather_man_prediction=request.POST['weather_man_prediction']
            outcomes=request.POST['outcome']

            if prediction == "Yes":
                prediction = True
            else:
                prediction = False

            if weather_man_prediction == "Yes":
                weather_man_prediction = True
            else:
                weather_man_prediction = False

            if outcomes == "Yes":
                outcomes = True
            else:
                outcomes = False
            prediction_id = request.POST.get("prediction_id")
            print("prediction_id : ", prediction_id)
            # objP is an instance for Prediction model.
            if prediction_id:
                objP = Prediction.objects.get(pk=int(prediction_id))
                action_type = "update"
            else:
                objP = Prediction()
                action_type = "saved"
                
            objP.user = request.user
            # objP.name = name
            objP.prediction = prediction
            objP.weather_man_prediction = weather_man_prediction
            objP.outcomes = outcomes
            objP.save()
            success_status = True
            msg = "Your prediction " + str(action_type) + " successfully."
            return redirect("/prediction/list")
        except:
            error_status = True
            msg = "Error occured while submitting prediction."
            
    return render(request, 'admin/add_prediction.html', {
        "objP": objP,
        "prediction_id": prediction_id,
        "msg": msg,
        "success_status": error_status,
        "success_status": success_status,
    })

@login_required
def admin_delete_predict(request):
    msg = ""
    error_status = False
    success_status = False
    response_data = {}
    if request.method=='POST':
        try:
            prediction_id = request.POST['prediction_id']
            Prediction.objects.get(pk=int(prediction_id)).delete()
            success_status = True
            response_data['msg'] = "Prediction delete successfully."
            response_data['success_status'] = success_status
        except:
            error_status = True
            response_data['msg'] = "Error occured while delete prediction."
            response_data['error_status'] = error_status

    return HttpResponse(json.dumps(response_data), content_type="application/json")

@login_required
def predict_list(request):
    return render(request, 'prediction_list.html', {
        "predictions": Prediction.objects.filter(user=request.user)
    })
import sys
@login_required
def predict_add(request):
    msg = ""
    error_status = False
    success_status = False
    if request.method=='POST':
        try:
            # name=request.POST['name']
            # date=request.POST['date']
            prediction=request.POST['prediction']
            weather_man_prediction=request.POST['weather_man_prediction']
            outcomes=request.POST['outcome']

            if prediction == "Yes":
                prediction = True
            else:
                prediction = False

            if weather_man_prediction == "Yes":
                weather_man_prediction = True
            else:
                weather_man_prediction = False

            if outcomes == "Yes":
                outcomes = True
            else:
                outcomes = False
            
            # objP is an instance for Prediction model.
            objP = Prediction()
            objP.user = request.user
            # objP.name = name
            objP.prediction = prediction
            objP.weather_man_prediction = weather_man_prediction
            objP.outcomes = outcomes
            objP.save()
            success_status = True
            msg = "Your prediction submit successfully."
            return redirect("/prediction/list")
        except:
            print(sys.exc_info())
            error_status = True
            msg = "Error occured while submitting prediction."

    return render(request, 'add_prediction.html', {
        "msg": msg,
        "success_status": error_status,
        "success_status": success_status,
    })

@login_required
def predict_delete(request):
    msg = ""
    error_status = False
    success_status = False
    response_data = {}
    if request.method=='POST':
        try:
            prediction_id = request.POST['prediction_id']
            Prediction.objects.get(pk=int(prediction_id)).delete()
            success_status = True
            response_data['msg'] = "Prediction delete successfully."
            response_data['success_status'] = success_status
        except:
            error_status = True
            response_data['msg'] = "Error occured while delete prediction."
            response_data['error_status'] = error_status

    return HttpResponse(json.dumps(response_data), content_type="application/json")

def predict_show(request):
    list_prediction=Prediction.objects.all()
    context={'list_prediction': list_prediction}
    return render(request, 'home/show.html', context)

