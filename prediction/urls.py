from django.conf.urls import url
from . import views

urlpatterns = [
    url("^list/", views.predict_list, name="Prediction List"),
    url("^add/", views.predict_add, name="Prediction Add"),
    url("^delete/", views.predict_delete, name="Prediction Delete"),
    url("^show/", views.predict_show, name="Prediction Show"),

    # Admin Section
    url("^admin/list/", views.admin_list_prediction, name="Admin List Prediction"),
    url("^admin/add-update/", views.admin_add_update_prediction, name="Admin Update Prediction"),
    url("^admin/delete/", views.admin_delete_predict, name="Admin Delete Prediction"),
]