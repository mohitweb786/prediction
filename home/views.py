from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from prediction.models import Prediction
from django.utils import timezone
import datetime, json

# Create your views here.

@login_required
def index(request):
    context = {}
    data = {
        'items': []
    }
    # admin_user = False
    # if request.user.is_superuser:
    #     admin_user = True
    admin_user = User.objects.filter(is_superuser=True)
    if len(admin_user):
        admin_user = admin_user[0]

    data["userId"] = int(request.user.id)
    # data["userName"] = request.user.username
    u = len(Prediction.objects.filter(user=request.user))
    t = len(Prediction.objects.all())
    data["overall_percentage"] = u * 100 / t
    for i in range(0, 15):
        old_date = timezone.now()-datetime.timedelta(i)
        old_date = str(old_date).split()[0]
        print("old_date : ",old_date)
        start_date = old_date + " 00:00:00"
        end_date = old_date + " 23:59:59"
        user_prediction = Prediction.objects.filter(user=request.user, created_at__range=[start_date, end_date])
        admin_prediction = Prediction.objects.filter(user=admin_user, created_at__range=[start_date, end_date])
        users_prediction = Prediction.objects.filter(created_at__range=[start_date, end_date])
        if len(users_prediction):
            percentage_by_day = int(1 / len(users_prediction) * 100)
        else:
            percentage_by_day = 0
        
        # if request.user.is_authenticated():
        #     predictions = Prediction.objects.filter(user=request.user, created_at__range=[start_date, end_date])
        # else:
        #     predictions = Prediction.objects.filter(created_at__range=[start_date, end_date])
        
        is_user_prediction = False
        if len(user_prediction):
            is_user_prediction = True

        is_admin_prediction = False
        if len(admin_prediction):
            is_admin_prediction = True

        # admin_user_prediction = False
        # if admin_user:
        #     admin_predictions = Prediction.objects.filter(user=admin_user[0], created_at__range=[start_date, end_date])
        #     if len(admin_predictions):
        #         admin_user_prediction = True
                
        data['items'].append({
            "is_user_prediction": is_user_prediction,
            "is_admin_prediction": is_admin_prediction,
            "percentage_by_day": percentage_by_day,
            "date": old_date
        })
    
    return render(request, 'index.html', {"predictions": json.dumps(data)})
